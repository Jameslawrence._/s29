//jshint esversion: 6

const express = require('express');

const app = express();
const port = 4000; 

app.use(express.json());
app.use(express.urlencoded({extended: true}));

let users = [];

app.get('/home', (req, res) => {
	res.send(`Welcome to the Home Page`);
})

app.get('/users', (req, res) => {
	res.send(users);
})

//POST METHOD
app.post('/register',(req, res) => {
	const newUser = req.body;
	let currentDate = new Date();
	let Users = new Object();
	Users.id = '_' +Math.random().toString(36).substr(2, 9)
	Users.username = newUser.username
	Users.email = newUser.email
	Users.password = newUser.password
	Users.isActive = true 
	Users.createdDate = currentDate
	let isTaken = users.find( user => Users.username === user.username)
	if(isTaken === undefined){
		users.push(Users)
		res.status(201).json(users)
	}else{
		res.status(404).json(`Username is already taken`)	
	}
})

//DELETE METHOD 
app.delete('/delete-user/:id', (req,res) => {
	const id = req.params.id;
	const userFound = users.find( user => user.id === id)
	console.log(userFound)
	if(userFound){
		let usersFiltered = users.filter( user => user.id !== id)
		res.status(201).json(usersFiltered)
	}else{
		res.status(404).json(`User does not exist!`)
	}
})

//UPDATE METHOD - update the username of the user.
app.put('/update-user/:id', (req, res) => {
	const { id } = req.params;
	const userUpdate = req.body;
	console.log(userUpdate);
	const index = users.findIndex( user => user.id === id)

	if(index !== -1){
		users[index] = userUpdate;
		res.status(200).json(users);
	}else{
		res.status(404).json(`User does not exit`);
	}
})


/*
	Object assign 
*/
app.patch('/update-user-patch/:id', (req, res) => {
	const { id } = req.params;
	let changes = req.body;
	const found = users.find( user => user.id === id)

	if(found){
		Object.assign(found, changes);
		res.status(200).json(users);
	}else{
		res.status(404).json(`User does not exist`);
	}
})

//PATCH METHOD  


app.listen(port, () => {
	console.log(`Server is running at port ${port}`);
})

